# Trombi Promo 2019-2020

Projet pour travailler les workflows Git sur Gitlab.

# Réalisons un Trombi

1. Créer une issue sur le projet pour demander à rajouter votre photo
2. S'attribuer l'issue
3. Travailler sur un fork
4. Faire la modification
5. Créer la Merge Request
6. Demander la validation de Merge Request
7. Fermer l'issue dès que c'est terminé

# Workflow Git / Gitlab

## 💻 Développeur 

1. Travailler sur son fork.
2. Choisir une tâche (une issue), se l'approprier et discuter avec la personne à l'initiative de celle-ci afin de savoir ce qu'il faut faire.
3. Créer une branche pour cette tâche et une merge request préfixée par "WIP:" pour montrer que le travail est en cours.
4. Travailler sur la tâche.
3. Quand le travail est terminé, enlever le préfix WIP et l'attribuer à/aux personnes concernées.
4. Suivre les commentaires faits sur la merge request et répondre rapidement aux sollicitations. Attention à ne pas fermer les conversations. C'est celui qui a créé la conversation qui doit la clore.

### Bonne conduite

- Une MR ne doit idéalement pas dépasser les 5 fichiers modifiés sauf raison valable
- Le sujet de la MR doit définir clairement ce que fait la MR
- Si on retravaille une MR, ou qu'elle n'est pas terminée, préfixer le sujet avec `WIP: `
  *(et enlever le préfixe une fois que c'est terminé)*
- La description de la MR doit décrire tout ce que le titre ne décrit pas
- Les commits doivent avoir des messages explicites
- Les commits doivent au maximums être organisés par ensemble cohérent de modifications

## 📑 Relecteur

1. Mettre une icône personnelle pour indiquer qu'il a commencé à lire la MR.
2. Ouvrir des discussions en cas de besoins.
3. Répondre aux réponses du développeur et fermer chaque discussion quand elle est résolue.
4. Quand toutes les discussions sont résolues et que la MR lui convient, mettre un :thumbsup: sur la MR.

### Bonne conduite

- Poser des questions et ne pas imposer les modifications
- Vérifier qu'il y a les tests nécessaires pour la MR
- Tout le monde peut être relecteur

## 💂‍ Gardien du dépôt

1. Relancer les relecteurs et les développeurs afin qu'une merge request soit traitée au plus vite.
2. Relancer le développeur en cas de conflit ou de soucis lors du CI.
2. Quand deux pouces (à minima et si possible) sont sur la merge request, que toutes les discussions sont fermées et que les pipelines passent, il peut valider la MR.
3. Surveiller que les issues évoluent correctement.



